import React from "react";
import Banner from "../../components/banner/Banner";

export default function HomePage() {
  return (
    <div>
      <Banner />
    </div>
  );
}
