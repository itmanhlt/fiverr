import React from "react";
import { Carousel } from "antd";
import { Input } from "antd";
const { Search } = Input;

const contentStyle = {
  margin: 0,
  height: "760px",
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
  backgroundImage: `url("https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto,dpr_1.0/v1/attachments/generic_asset/asset/4637ac0b5e7bc7f247cd24c0ca9e36a3-1690384616487/jenny-2x.jpg")`,
};
const contentStyle1 = {
  margin: 0,
  height: "760px",
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
  backgroundImage: `url("https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto,dpr_1.0/v1/attachments/generic_asset/asset/4637ac0b5e7bc7f247cd24c0ca9e36a3-1690384616493/colin-2x.jpg")`,
};
const contentStyle2 = {
  margin: 0,
  height: "760px",
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
  backgroundImage: `url("https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto,dpr_1.0/v1/attachments/generic_asset/asset/4637ac0b5e7bc7f247cd24c0ca9e36a3-1690384616493/jordan-2x.jpg")`,
};

export default function Banner() {
  const onChange = (currentSlide) => {
    // console.log(currentSlide);
  };
  return (
    <div>
      <div className="w-[650px] absolute top-1/4 left-[251px] z-50">
        <h1 className="text-[48px] leading-[56px] text-white font-bold">
          Find the right <i>freelance</i> service, right away
        </h1>
        <div className="py-7">
          <Search
            placeholder="Search for any service..."
            enterButton="Search"
            size="large"
          />
        </div>
        <div className="flex items-center space-x-4 text-white text-[14px]">
          <p>Popular:</p>
          <button className="px-3 py-1 border rounded-3xl hover:bg-white hover:text-black duration-500">
            Website desIgn
          </button>
          <button className="px-3 py-1 border rounded-3xl hover:bg-white hover:text-black duration-500">
            WordPress
          </button>
          <button className="px-3 py-1 border rounded-3xl hover:bg-white hover:text-black duration-500">
            Logo DesIgn
          </button>
          <button className="px-3 py-1 border rounded-3xl hover:bg-white hover:text-black duration-500">
            AI Services
          </button>
        </div>
      </div>
      <Carousel autoplay="true" dots="false" afterChange={onChange}>
        <div>
          <h3 style={contentStyle}></h3>
        </div>
        <div>
          <h3 style={contentStyle1}></h3>
        </div>
        <div>
          <h3 style={contentStyle2}></h3>
        </div>
      </Carousel>
    </div>
  );
}
