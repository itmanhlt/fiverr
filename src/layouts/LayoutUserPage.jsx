import React from "react";
import Header from "../components/header/Header";
import Footer from "../components/footer/Footer";

export default function ({ Componnets }) {
  return (
    <div className="flex flex-col h-full min-h-screen ">
      <Header />
      <div className="flex-grow">
        <Componnets />
      </div>
      <Footer />
    </div>
  );
}
