import LayoutUserPage from "../layouts/LayoutUserPage";
import HomePage from "../pages/users/HomePage";

export const routes = [
  {
    url: "/",
    component: <LayoutUserPage Componnets={HomePage} />,
  },
];
